package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.error.BookIdMismatchException;
import com.example.demo.error.BookNotFoundException;
import com.example.demo.model.Book;
import com.example.demo.repository.BookRepository;

@RestController
@RequestMapping("/api/books")
public class BookController {

	 @Autowired
	    private BookRepository bookRepository;

	    @GetMapping
	    public Iterable findAll() {
	        return bookRepository.findAll();
	    }

	    @GetMapping("/title/{bookTitle}")
	    public List findByTitle(@PathVariable String bookTitle) {
	        return bookRepository.findByTitle(bookTitle);
	    }

	    @GetMapping("/{id}")
	    public Book findOne(@PathVariable Long id) {
	        return bookRepository.findById(id).orElseThrow(null);
	          //.orElseThrow(BookNotFoundException::new);
	    }

	    @PostMapping
	    @ResponseStatus(HttpStatus.CREATED)
	    public Book create(@Valid @RequestBody Book book) {
	        return bookRepository.save(book);
	    }
	    
	    @DeleteMapping("/{id}")
	    public void delete(@PathVariable Long id) {
	        bookRepository.findById(id)
	          .orElseThrow(null);
	        bookRepository.deleteById(id);
	    }

	    @PutMapping("/{id}")
	    public Book updateBook(@Valid @RequestBody Book book, @PathVariable Long id) {
	        if (book.getId() != id) {
	          throw new BookIdMismatchException("Book Id not match");
	        }
	        bookRepository.findById(id)
	          .orElseThrow(null);
	        return bookRepository.save(book);
	    }
	    
	    
}
