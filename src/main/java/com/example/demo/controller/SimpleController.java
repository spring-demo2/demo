package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.model.Book;
import com.example.demo.repository.BookRepository;

@Controller
public class SimpleController {
	@Value("${spring.application.name}")
    String appName;

	 @Autowired
	    private BookRepository bookRepository;
	 
    @GetMapping("/home")
    public String homePage(Model model) {
        model.addAttribute("appName", appName);
        return "index";
    }
    
    @GetMapping("/add")
    public String addPage(Model model) {
    	model.addAttribute("book", new Book());
    	return "add-book";
    }
    
    @PostMapping("/addbook")
    public String addUser(@Valid Book book, BindingResult result, Model model) {
    	System.out.println("add book");
        if (result.hasErrors()) {
            return "add-book";
        }
        
        bookRepository.save(book);
        return "redirect:/index";
    }
    
    
    @GetMapping("/index")
    public String showBookList(Model model) {
        model.addAttribute("books", bookRepository.findAll());
        return "index";
    }
    
    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Book book = bookRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        
        model.addAttribute("book", book);
        return "update-book";
    }
    
    @PostMapping("/update/{id}")
    public String updateBook(@PathVariable("id") long id,  Book book, 
      BindingResult result, Model model) {
        if (result.hasErrors()) {
            book.setId(id);
            return "update-book";
        }
            
       bookRepository.save(book);
        return "redirect:/index";
    }
    
    @GetMapping("/delete/{id}")
    public String deleteBook(@PathVariable("id") long id, Model model) {
        Book book = bookRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        bookRepository.delete(book);
        return "redirect:/index";
    }
}
