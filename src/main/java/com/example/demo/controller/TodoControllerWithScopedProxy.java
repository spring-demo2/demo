package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.model.TodoItem;
import com.example.demo.model.TodoList;

@Controller
@RequestMapping("/scopedproxy")
public class TodoControllerWithScopedProxy {
	 private TodoList todos;

	public TodoControllerWithScopedProxy(TodoList todos) {
		super();
		this.todos = todos;
	}
	 
	 
	@GetMapping("/form")
	public String showForm(Model model) {
	    if (!todos.isEmpty()) {
	        model.addAttribute("todo", todos.peekLast());
	    } else {
	        model.addAttribute("todo", new TodoItem());
	    }
	    return "scopedproxyform";
	}
}
