package com.example.demo.error;

public class BookIdMismatchException extends RuntimeException{

	public BookIdMismatchException(String message) {
        super(message);
    }
}
