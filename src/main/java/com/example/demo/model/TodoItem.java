package com.example.demo.model;

import java.time.LocalDateTime;

public class TodoItem {
	private String description;
	private LocalDateTime createDate;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

}
